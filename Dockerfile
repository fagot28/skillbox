FROM node

RUN mkdir /skillbox
WORKDIR /skillbox
COPY ./apps/package.json /skillbox
RUN yarn install

COPY ./apps /skillbox

RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000

